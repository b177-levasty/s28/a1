// #3

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((json) => console.log(json));

// #4

fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'GET',
	headers: {
		'Content-type': 'application/json'
	},
})
.then(response => response.json())
.then(json => {
   json.map((data) => {
    console.log(data.title)
  })
})

// #5
fetch('https://jsonplaceholder.typicode.com/posts/22')
.then((response) => response.json())
.then((json) => console.log(json));

// #6
fetch('https://jsonplaceholder.typicode.com/posts?title=aut amet sed', {
		method: 'GET',
	headers: {
		'Content-type': 'application/json'
	},
})
.then(response => response.json())
.then(json => {
   json.map((data) => {
    console.log(`The title is ${data.title}`)
  })
})

// #7

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		status: 'Pending',
		title: 'REST API Activity #1',
		body: 'All about REST API'
	}),
})
	.then((response) => response.json())
	.then((json) => console.log(json));

// #8

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT'
})

// #9

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
	Title: 'Updated Title',
	Description: 'Step 9',
	Status: 'Updated status',
	dateCompleted: true,
	UserId: 1
	}),
})
	.then((response) => response.json())
	.then((json) => console.log(json));


// #10

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH'
})

// #11

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		status: 'Complete'
	}),
})
	.then((response) => response.json())
	.then((json) => console.log(json));

// #12

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((json) => console.log(json));

/*
13. Create a request via Postman to retrieve all the to do list items.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as get all to do list items

14. Create a request via Postman to retrieve an individual to do list item.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as get to do list item

15. Create a request via Postman to create a to do list item.
- POST HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as create to do list item

16. Create a request via Postman to update a to do list item.
- PUT HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as update to do list item PUT
- Update the to do list item to mirror the data structure used in the PUT fetch request

17. Create a request via Postman to update a to do list item.
- PATCH HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as create to do list item
- Update the to do list item to mirror the data structure of the PATCH fetch request

18. Create a request via Postman to delete a to do list item.
- DELETE HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as delete to do list item

19. Export the Postman collection and save it in the activity folder.

20. Create a gitlab project repository named a1 in s28.

21. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.

*/

// Below is #13 to #18 may ginawa din po ako sa postman

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Conrad Levasty',
		body: 'REST API Activity',
		userID: 1
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated post',
		body: 'Hello again!',
		userId: 1
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		title: "Conrad Pogi"
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((json) => console.log(json));









